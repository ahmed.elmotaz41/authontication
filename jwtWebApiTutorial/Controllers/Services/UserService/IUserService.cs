namespace jwtWebApiTutorial.Controllers.Services.UserService
{
    public interface IUserService
    {
        string GetMyName();
    }
}